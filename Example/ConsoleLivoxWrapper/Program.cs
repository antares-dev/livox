﻿using LivoxSdk.Def;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleLivoxWrapper
{

    class Eje
    {
        delegate void DeviceStateUpdateDelegate(string saludo, int valor);


        DeviceStateUpdateDelegate onDeviceStateUpdate;

        /// public	event DeviceStateUpdateDelegate OnDeviceStateUpdateAsync
        /// {
        ///     void add(DeviceStateUpdateDelegate eventHandler)
        /// 	{
        ///         onDeviceStateUpdate += eventHandler;
        ///     }
        /// 
        ///     void remove(DeviceStateUpdateDelegate  eventHandler)
        /// 	{
        ///         onDeviceStateUpdate -= eventHandler;
        ///     }
        /// }


        void Raise(string saludo, int valor)
        {
            onDeviceStateUpdate.BeginInvoke(saludo, valor, callback, null);
        }

        private void callback(IAsyncResult ar)
        {
            var result = (AsyncResult)ar;
            EventHandler caller = (EventHandler)result.AsyncDelegate;
            onDeviceStateUpdate.EndInvoke(ar);
        }
    }


    class Program
    {
        static LivoxSdk.Livox livox = LivoxSdk.Livox.GetInstance();

        unsafe static void Main(string[] args)
        {

            var lvLivoxRawPoint = livox.GetLivoxRawPointDemo();


            if (true)
            {

                int count = 0;
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var result1 = livox.GetLivoxRawPointDemoNative(20000);
                count += result1.Length;
                result1 = livox.GetLivoxRawPointDemoNative(20000);
                count += result1.Length;
                result1 = livox.GetLivoxRawPointDemoNative(20000);
                count += result1.Length;
                result1 = livox.GetLivoxRawPointDemoNative(10000);
                count += result1.Length;
                watch.Stop();
                var elapsed = watch.Elapsed.TotalSeconds;

                Console.WriteLine("GetLivoxRawPointDemoNative  count:{0} elapsed:{1}", count, elapsed);


                /// int count2 = 0;
                /// var watch2 = System.Diagnostics.Stopwatch.StartNew();
                /// var result2 = livox.GetLivoxRawPointDemoAdmin(20000);
                /// count2 += result2.Length;
                /// result2 = livox.GetLivoxRawPointDemoAdmin(20000);
                /// count2 += result2.Length;
                /// result2 = livox.GetLivoxRawPointDemoAdmin(20000);
                /// count2 += result2.Length;
                /// result2 = livox.GetLivoxRawPointDemoAdmin(10000);
                /// count2 += result2.Length;
                /// watch2.Stop();
                /// var elapsed2 = watch2.Elapsed.TotalSeconds;
                /// 
                /// Console.WriteLine("GetLivoxRawPointDemoAdmin count2:{0} elapsed:{1}", count2, elapsed2);

                int count3 = 0;
                var watch3 = System.Diagnostics.Stopwatch.StartNew();
                var result3 = livox.GetLivoxRawPointDemoNative2(20000);
                count3 += result3.Length;
                result3 = livox.GetLivoxRawPointDemoNative2(20000);
                count3 += result3.Length;
                result3 = livox.GetLivoxRawPointDemoNative2(20000);
                count3 += result3.Length;
                result3 = livox.GetLivoxRawPointDemoNative2(10000);
                count3 += result3.Length;
                watch3.Stop();
                var elapsed3 = watch3.Elapsed.TotalSeconds;

                Console.WriteLine("GetLivoxRawPointDemoNative2 count:{0} elapsed:{1}", count3, elapsed3);



                var version = livox.LvGetLivoxSdkVersion();
                Console.WriteLine($"Livox Version: {version.major}.{version.minor}.{version.patch}");
            }

            // livox.LvLidarStartAutoConnect(null);


            var t = livox.HubStartSamplingAsync();

            Thread.Sleep(3);
            livox.HubStartSamplingAsync();
            Thread.Sleep(3);
            livox.HubStartSamplingAsync();

            // t.Wait(50000);

            livox.OnDeviceBroadcast += (info) =>
            {
                if (info.dev_type == WrLvDeviceType.kDeviceTypeLidarMid40)
                {
                    livox.LvAddLidarToConnect(info.broadcast_code);
                }
            };

            
            livox.OnDeviceStateUpdate += (deviceInfo, eventType) =>
            {
                if (eventType == WrLvDeviceEvent.kEventConnect)
                {
                    //  Task.Run(() => {

                    var device = livox.GetDeviceByHandle(deviceInfo.handle);

                    device.OnStateUpdate += Device_OnStateUpdate;
                    device.OnErrorMessage += Device_OnErrorMessage;
                    device.OnReceivePoints += Device_OnReceivePoints;
                    device.OnReceiveRawPoint += Device_OnReceiveRawPoint;
                    device.OnReceiveSpherPoint += Device_OnReceiveSpherPoint; 

                    device.LidarSetUtcSyncTimeAsync(DateTime.UtcNow).Wait();
                    var coord = device.SetSphericalCoordinateAsync();
                    var sampling = device.LidarStartSamplingAsync();

                    Console.WriteLine("SetSphericalCoordinateAsync {0}", coord.Result);
                    Console.WriteLine("LidarStartSamplingAsync {0}", sampling.Result);

                    //  });
                }
            };

            livox.LvInit();

            livox.LvStart(); 

            Console.ReadKey();


           

            livox.LvUninit();
        }
         

        private static void Livox_OnDeviceStateUpdateAsync(WrLvDeviceInfo deviceInfo, WrLvDeviceEvent eventType)
        {
            throw new NotImplementedException();
        }

        private static void safdafdsf(WrLvDeviceInfo deviceInfo, WrLvDeviceEvent eventType)
        {
            throw new NotImplementedException();
        }

        private static void asdfasdf(uint status, byte handle, byte response)
        {
            throw new NotImplementedException();
        }

        private static void Device_OnReceivePoints(LivoxSdk.LivoxDevice device, IntPtr livoxEthPacketData, uint dataCount)
        {
            //  var livoxRawPoints = (WrLvLivoxRawPoint[])device.ProcessReceiveData(livoxEthPacketData, dataCount);

        }

        private static void Device_OnStateUpdate(LivoxSdk.LivoxDevice device, WrLvDeviceEvent eventType)
        {
            var s = device.GetState();
            var connected = device.IsConnected();
            Console.WriteLine($"Device_OnStateUpdate connected:{connected} eventType:{eventType}");

        }

        private static void Device_OnErrorMessage(LivoxSdk.LivoxDevice device, int status, uint errorMessage)
        {
            Console.WriteLine($"Device_OnErrorMessage errorMessage:{errorMessage}");
        }

        private static void Device_OnReceiveRawPoint(LivoxSdk.LivoxDevice device, DateTime arrivedTime, LvLivoxRawPoint[] points)
        {
            Console.WriteLine($"Device_OnReceiveRawPoint points:{points.Length}");
        }

        private static void Device_OnReceiveSpherPoint(LivoxSdk.LivoxDevice device, DateTime arrivedTime, LvLivoxSpherPoint[] points)
        {
            Console.WriteLine($"OnReceiveSpherPoint points:{points.Length}");
        }

    }
}
