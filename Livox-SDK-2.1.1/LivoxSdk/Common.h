#pragma once
 
#include "LivoxSdkDef.h"
#include <memory.h>

using namespace System;
using namespace System::Net;

namespace LivoxSdk {
    using namespace LivoxSdk::Def;
    using namespace System::Runtime::InteropServices;


    ref class Common
    {
    public:

        template <typename TAdmin, typename TNative>
        static array<TAdmin^>^ ParseData(TNative src, uint32_t data_num)
        {
            auto d = gcnew array<TAdmin^>(data_num);
            for (uint32_t i = 0; i < data_num; i++)
            {
                d[i] = gcnew TAdmin(src);
                src++;
            }
            return d;
        }


        template <typename TStructAdmin>
        static array<TStructAdmin>^ MarshalStructCopy(IntPtr src, int byteLength)
        {
            int sizeStruct = sizeof(TStructAdmin);
            int arrayLength = byteLength / sizeStruct;
             
            auto result = gcnew array<TStructAdmin>(arrayLength);

            GCHandle handle = GCHandle::Alloc(result, GCHandleType::Pinned);
            auto ptrResult = handle.AddrOfPinnedObject(); 

            memcpy((void*)ptrResult, (void*)src, arrayLength * sizeStruct);

            handle.Free(); 

            return  result;
        }
         

        static int AddreesToInt(IPAddress^ ipaddress)
        {
            int ip = ipaddress->Address;
            return IPAddress::NetworkToHostOrder(ip);
        }
    }; 
}
