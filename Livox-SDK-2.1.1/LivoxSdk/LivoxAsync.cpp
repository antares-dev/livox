#include "pch.h"

#include "LivoxSdk.h"
#include "LivoxDevice.h"
#include <stdio.h>
#include "Common.h"
#include <memory.h>
 

namespace LivoxSdk {

	using namespace LivoxSdk::Def;
	using namespace System;
	using namespace System::Threading;
	using namespace System::Threading::Tasks;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Collections::Concurrent;

	 

	void HubStartSamplingCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data; 
		Livox^ l = Livox::GetInstance();

		if (status == kStatusSuccess)
			l->TasksSource->TrySetResult<uint8_t>(clientId, response);
		else
			l->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status); 
	}


	Task<uint8_t>^ Livox::HubStartSamplingAsync()
	{  
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (HubStartSampling(HubStartSamplingCb, (void *)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	} 


	void HubStopSamplingCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data; 
		Livox^ l = Livox::GetInstance();

		if (status == kStatusSuccess)
			l->TasksSource->TrySetResult<uint8_t>(clientId, response);
		else
			l->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status); 
	}


	Task<uint8_t>^ Livox::HubStopSamplingAsync()
	{  
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (HubStopSampling(HubStopSamplingCb, (void *)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);	
		}
		return task;
	}
	

#pragma region Pruebas Rendimiento
	 
	LivoxRawPoint* GeneratePoints(int len)
	{
		LivoxRawPoint* points = new LivoxRawPoint[len];

		for (int i = 0; i < len; i++)
		{
			points[i].reflectivity = 0;
			points[i].x = i % 100;
			points[i].y = 100 - i % 100;
			points[i].x = 200 - i % 200;
		}
		return points;
	}

	array<Def::LvLivoxRawPoint>^ Livox::GetLivoxRawPointDemoNative(int len)
	{
		  LivoxRawPoint* points = GeneratePoints(len);
		  auto result = Common::MarshalStructCopy<Def::LvLivoxRawPoint>((IntPtr)points, sizeof(LivoxRawPoint) * len); 
		  delete points;
		  return result;
	}
	
	array<Def::LvLivoxRawPoint>^ Livox::GetLivoxRawPointDemoNative2(int len)
	{
		  LivoxRawPoint* points = GeneratePoints(len);

		  IntPtr src = (IntPtr)points;
		  int byteLength = sizeof(LivoxRawPoint) * len;
		  int sizeStruct = sizeof(Def::LvLivoxRawPoint);
		  int arrayLength = byteLength / sizeStruct;

		  // auto data = gcnew array<Byte>(byteLength);
		  auto result = gcnew array<Def::LvLivoxRawPoint>(arrayLength);

		  GCHandle handle = GCHandle::Alloc(result, GCHandleType::Pinned);
		  auto ptrResult = handle.AddrOfPinnedObject();

		  // Marshal::Copy(src, data, 0, byteLength);
		  // Marshal::Copy(data, 0, ptrResult, byteLength);
		  memcpy((void*)ptrResult, (void*)src, arrayLength * sizeStruct);

		  handle.Free();
		  // delete data;

		  delete points;
		  return  result;

	}

	/// array<Def::WrLvLivoxRawPoint^>^ Livox::GetLivoxRawPointDemoAdmin(int len)
	/// {
	/// 	LivoxRawPoint* points = GeneratePoints(len);
	/// 
	/// 	auto result= Common::ParseData<Def::WrLvLivoxRawPoint, LivoxRawPoint*>((LivoxRawPoint*)points, len);
	/// 
	/// 	delete points;
	/// 	return result;
	/// }




	array<Def::LvLivoxRawPoint>^ Livox::GetLivoxRawPointDemo()
	{
		using namespace System::Runtime::InteropServices;

		LivoxRawPoint point;
		point.x = 34;
		point.y = -4;
		point.z = 89;
		point.reflectivity = 12;

	

		IntPtr src = (IntPtr)&point;
		int byteLength = sizeof(LivoxRawPoint); 
		int sizeStruct = sizeof(Def::LvLivoxRawPoint);
		int arrayLength = byteLength / sizeStruct;

		// auto data = gcnew array<Byte>(byteLength);
		auto result = gcnew array<Def::LvLivoxRawPoint>(arrayLength);

		GCHandle handle = GCHandle::Alloc(result, GCHandleType::Pinned);
		auto ptrResult = handle.AddrOfPinnedObject();
		 
		// Marshal::Copy(src, data, 0, byteLength);
		// Marshal::Copy(data, 0, ptrResult, byteLength);
		memcpy((void*)ptrResult, (void*)src, arrayLength * sizeStruct);

		handle.Free();
		// delete data;

		return  result;


		return Common::MarshalStructCopy<Def::LvLivoxRawPoint>((IntPtr)&point, sizeof(LivoxRawPoint));


		/// auto data = gcnew array<Byte>(sizeof(LivoxRawPoint)); 
		/// auto result = gcnew array<Def::LvLivoxRawPoint>(1);
		/// 
		/// GCHandle handle = GCHandle::Alloc(result, GCHandleType::Pinned);
		/// auto ptrResult = handle.AddrOfPinnedObject(); 
		/// Marshal::Copy((IntPtr)&point, data, 0, data->Length); 
		/// Marshal::Copy(data, 0, ptrResult, data->Length);
		/// handle.Free();
		///  
		/// return  result;
	}


	 

	// https://codereview.stackexchange.com/questions/137897/interop-between-c-and-c-via-c-cli-with-callbacks
	
	//// livox_status Livox::LvHubStopSampling()
	//// {
	//// 
	//// 	/// auto es = gcnew EventSourceCpp<OnResponseHubStopSamplingDelegate>();
	//// 	/// es->pE->Invoke(0, 0);
	//// 	/// es->add();
	//// 
	//// 	// using namespace System::Runtime::InteropServices; 
	//// 	// IntPtr stub_ptr = cb == nullptr ? IntPtr::Zero : Marshal::GetFunctionPointerForDelegate(cb);
	//// 	// return HubStopSampling((CommonCommandCallback)(stub_ptr.ToPointer()), nullptr);
	//// 	return HubStopSampling(HubStopSamplingCb2, nullptr);
	//// }

#pragma endregion

}