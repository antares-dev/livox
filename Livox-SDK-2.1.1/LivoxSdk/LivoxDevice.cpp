#include "pch.h"
#include "LivoxDevice.h"
#include "LivoxSdk.h"
#include "Common.h"
#include <stdio.h> 



namespace LivoxSdk {

	using namespace System::Threading::Tasks;
	using namespace System;
	using namespace System::Threading;
	using namespace System::Threading::Tasks;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Collections::Concurrent; 
	using namespace System::Runtime::InteropServices;

	LivoxDevice::LivoxDevice(int handle, String^ Id)
	{
		this->handle = handle;
		this->deviceId = Id;
		this->broadcastCode = (char*)(void*)Marshal::StringToHGlobalAnsi(Id);
		this->tasksToReply = gcnew ConcurrentDictionary<int, Object^ >();
	}

	livox_status LivoxDevice::LvAddLidarToConnect()
	{
		if (IsConnected())
		{
			return kStatusNotSupported;
		}

		uint8_t newHandle = 0;
		livox_status result = AddLidarToConnect(this->broadcastCode, &newHandle);
		if (result == kStatusSuccess && newHandle < kMaxLidarCount)
		{
			SetHandle(newHandle);
			SetDataCallback(handle, OnInstanceReciveDataCB, NULL);
			return kStatusSuccess;
		}
		return kStatusFailure;
	}

	livox_status LivoxDevice::LvAddHubToConnect()
	{
		if (IsConnected())
		{
			return kStatusNotSupported;
		}

		uint8_t newHandle = 0;
		livox_status result = AddHubToConnect(this->broadcastCode, &newHandle);
		if (result == kStatusSuccess && handle < kMaxLidarCount)
		{
			SetHandle(newHandle);
			SetDataCallback(handle, OnInstanceReciveDataCB, NULL);
			return kStatusSuccess;
		}
		return kStatusFailure;
	}

	uint8_t LivoxDevice::GetHandle()
	{
		return handle;
	}

	void LivoxDevice::SetHandle(uint8_t handle)
	{
		this->handle = handle;
		SetErrorMessageCallback(handle, OnInstanceErrorMessageCB);
	}

	bool LivoxDevice::ContainHandle()
	{
		return handle != 255;
	}

	String^ LivoxDevice::GetId()
	{
		return deviceId;
	}
	
	Def::WrLvDeviceInfo^ LivoxDevice::GetInfo()
	{
		return devInfo;
	}

	Def::WrLvDeviceState LivoxDevice::GetState()
	{
		return devState;
	}

	bool LivoxDevice::IsConnected()
	{
		return ContainHandle()
			&& (devState == Def::WrLvDeviceState::kDeviceStateConnect
				|| devState == Def::WrLvDeviceState::kDeviceStateSampling);
	}

	void LivoxDevice::DoErrorMessage(livox_status status, ErrorMessage* message)
	{
		if (onErrorMessageAsync != nullptr)
			onErrorMessageAsync->BeginInvoke(this, status, message->error_code, nullptr, nullptr);
	}


	void LivoxDevice::DoDeviceInformation(livox_status status, DeviceInformationResponse* ack, void* client_data)
	{ 
		if (status != kStatusSuccess) {
			printf("Device Query Informations Failed %d\n", status);
		}
		if (ack) {
			firmwareVersion = String::Format("{0}.{1}.{2}.{3}",
				ack->firmware_version[0], ack->firmware_version[1],
				ack->firmware_version[2], ack->firmware_version[3]);
			Console::WriteLine("firm ver: {0}\n", firmwareVersion);
		}

		if (onDeviceInformationAsync != nullptr)
			onDeviceInformationAsync->BeginInvoke(this, status, ack, nullptr, nullptr);
	}

	void LivoxDevice::DoStateUpdate(Def::WrLvDeviceInfo^ devInfo, Def::WrLvDeviceEvent eventType)
	{
		this->devInfo = devInfo;
		switch (eventType)
		{
		case LivoxSdk::Def::WrLvDeviceEvent::kEventConnect:
			QueryDeviceInformation(handle, OnInstanceDeviceInformationCB, NULL);
			if (devState == Def::WrLvDeviceState::kDeviceStateDisconnect)
			{
				devState = Def::WrLvDeviceState::kDeviceStateConnect;
			}
			break;
		case LivoxSdk::Def::WrLvDeviceEvent::kEventDisconnect:
			devState = Def::WrLvDeviceState::kDeviceStateDisconnect;
			break;
		}

		if (onStateUpdateAsync != nullptr)
			onStateUpdateAsync->BeginInvoke(this, eventType, nullptr, nullptr);
	};

	void LivoxDevice::DoLidarStartSampling(livox_status status, uint8_t response)
	{
		if (status == kStatusSuccess && response == 0 && IsConnected())
		{
			devState = Def::WrLvDeviceState::kDeviceStateSampling;
		}

		if (onStateUpdateAsync != nullptr)
			onStateUpdateAsync->BeginInvoke(this, Def::WrLvDeviceEvent::kEventStateChange, nullptr, nullptr);
	}


	void LivoxDevice::DoReceiveData(LivoxEthPacket* data, uint32_t data_num)
	{
		if (data != nullptr)
		{
			IntPtr ptr = IntPtr::IntPtr(data);

			OnReceivePoints(this, ptr, data_num);
			 
			uint8_t handle = HubGetLidarHandle(data->slot, data->id);
			if (handle >= kMaxLidarCount) {
				return;
			}
			 
			uint64_t cur_timestamp = *((uint64_t*)(data->timestamp));
			DateTime arrivedTime = DateTime::UtcNow;

			if (data->data_type == kCartesian)
			{ 
				auto points = Common::MarshalStructCopy<Def::LvLivoxRawPoint>((IntPtr)data->data, sizeof(LivoxRawPoint) * data_num);
				OnReceiveRawPoint(this, arrivedTime, points);
			}
			else if (data->data_type == kSpherical)
			{
				auto points = Common::MarshalStructCopy<Def::LvLivoxSpherPoint>((IntPtr)data->data, sizeof(LivoxSpherPoint) * data_num);
				OnReceiveSpherPoint(this, arrivedTime, points); 
			}
			else if (data->data_type == kExtendCartesian)
			{ 
				auto points = Common::MarshalStructCopy<Def::LvLivoxExtendRawPoint>((IntPtr)data->data, sizeof(LivoxExtendRawPoint) * data_num); 
				OnReceiveExtendRawPoint(this, arrivedTime, points);
			}
			else if (data->data_type == kExtendSpherical)
			{
				auto points = Common::MarshalStructCopy<Def::LvLivoxExtendSpherPoint>((IntPtr)data->data, sizeof(LivoxExtendSpherPoint) * data_num); 
				OnReceiveExtendSpherPoint(this, arrivedTime, points);
			}
			else if (data->data_type == kDualExtendCartesian)
			{
				auto points = Common::MarshalStructCopy<Def::LvLivoxDualExtendRawPoint>((IntPtr)data->data, sizeof(LivoxDualExtendRawPoint) * data_num); 
				OnReceiveDualExtendRawPoint(this, arrivedTime, points);
			}
			else if (data->data_type == kDualExtendSpherical)
			{
				auto points = Common::MarshalStructCopy<Def::LvLivoxDualExtendSpherPoint>((IntPtr)data->data, sizeof(LivoxDualExtendSpherPoint) * data_num); 
				OnReceiveDualExtendSpherPoint(this, arrivedTime, points);
			}
			else if (data->data_type == kImu)
			{
				auto points = Common::MarshalStructCopy<Def::LvLivoxImuPoint>((IntPtr)data->data, sizeof(LivoxImuPoint) * data_num); 
				OnReceiveImuPoint(this, arrivedTime, points);
			}
		}
	}

	/* 
	Object^ LivoxDevice::ProcessReceiveData(IntPtr livoxEthPacketData, uint32_t dataCount)
	{
		auto data = (LivoxEthPacket*)livoxEthPacketData.ToPointer();

		if (data != nullptr)
		{
			if (data->data_type == kCartesian)
			{
				return Common::ParseData<WrLvLivoxRawPoint, LivoxRawPoint*>((LivoxRawPoint*)data->data, dataCount);
			}
			else if (data->data_type == kSpherical)
			{
				return Common::ParseData<WrLvLivoxSpherPoint, LivoxSpherPoint*>((LivoxSpherPoint*)data->data, dataCount);
			}
			else if (data->data_type == kExtendCartesian)
			{
				return Common::ParseData<WrLvLivoxExtendRawPoint, LivoxExtendRawPoint*>((LivoxExtendRawPoint*)data->data, dataCount);
			}
			else if (data->data_type == kExtendSpherical)
			{
				return Common::ParseData<WrLvLivoxExtendSpherPoint, LivoxExtendSpherPoint*>((LivoxExtendSpherPoint*)data->data, dataCount);
			}
			else if (data->data_type == kDualExtendCartesian)
			{
				return Common::ParseData<WrLvLivoxDualExtendRawPoint, LivoxDualExtendRawPoint*>((LivoxDualExtendRawPoint*)data->data, dataCount);
			}
			else if (data->data_type == kDualExtendSpherical)
			{
				return Common::ParseData<WrLvLivoxDualExtendSpherPoint, LivoxDualExtendSpherPoint*>((LivoxDualExtendSpherPoint*)data->data, dataCount);
			}
			else if (data->data_type == kImu)
			{
				return Common::ParseData<WrLvLivoxImuPoint, LivoxImuPoint*>((LivoxImuPoint*)data->data, dataCount);
			}
		}
		return nullptr;
	}
	*/

}