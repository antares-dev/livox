#pragma once
 
#include "livox_sdk.h"
#include "LivoxSdkDef.h"
#include "TaskSourceHelper.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Collections::Concurrent;
using namespace System::Threading::Tasks;
using namespace System::Net;


namespace LivoxSdk {

	public ref class LivoxDevice
	{ 

	protected: 
		ConcurrentDictionary<int,  Object^ >^ tasksToReply;
		String^ firmwareVersion; 

		uint8_t handle = -1;
		String^ deviceId;
		char* broadcastCode; 
		Def::WrLvDeviceInfo^ devInfo = nullptr;  
		Def::WrLvDeviceState devState = Def::WrLvDeviceState::kDeviceStateDisconnect;
		  
	internal:  
		LivoxDevice(int handle, String^ deviceId);

		TaskSourceHelper^ TasksSource = gcnew TaskSourceHelper(); 

		void DoStateUpdate(Def::WrLvDeviceInfo^ devInfo, Def::WrLvDeviceEvent eventType);
		void DoLidarStartSampling(livox_status status, uint8_t response);
		void DoReceiveData(LivoxEthPacket* data, uint32_t data_num); 
		void DoDeviceInformation(livox_status status, DeviceInformationResponse* ack, void* client_data);  
		void DoErrorMessage(livox_status status, ErrorMessage* message);


		void SetHandle(uint8_t handle);

		livox_status LvAddLidarToConnect();

		livox_status LvAddHubToConnect();

	public:   

		/// <summary>
		/// Get handle index on sdk stack
		/// </summary>
		/// <returns></returns>
		uint8_t GetHandle();
		
		/// <summary>
		/// Contain handle;
		/// </summary>
		/// <returns>true if device have been initialized on sdk</returns>
		bool ContainHandle();

		/// <summary>
		/// Obtain broadcast code (device id)
		/// </summary>
		/// <returns></returns>
		String^ GetId();

		/// <summary>
		/// Get Device Info struct
		/// </summary>
		/// <returns></returns>
		Def::WrLvDeviceInfo^ GetInfo();

		/// <summary>
		/// Get current state of device
		/// </summary>
		/// <returns></returns>
		Def::WrLvDeviceState GetState();

		/// <summary>
		/// Device is Connected  (state == kDeviceStateConnect)
		/// </summary>
		/// <returns></returns>
		bool IsConnected(); 

		/// <summary>
		/// Start LiDAR sampling.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ LidarStartSamplingAsync();

		/// <summary>
		/// Stop LiDAR sampling.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ LidarStopSamplingAsync();

		/// <summary>
		/// Set device's static IP mode.
		/// @note Mid40 / 100 is not supported to set subnet mask and gateway address.
		/// setting: net_mask and gw_addr will not take effect on Mid40/100.
		/// </summary>
		/// <param name="ipAddr"></param>
		/// <param name="netMask"></param>
		/// <param name="gwAddr"></param>
		/// <returns></returns>
		Task<uint8_t>^ SetStaticIpAsync(IPAddress^ ipAddr,  IPAddress^ netMask,  IPAddress^ gwAddr);

		/// <summary>
		/// Set device's dynamic IP mode.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ SetDynamicIpAsync();
		  
		/// <summary>
		/// Disconnect divice.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ DisconnectDeviceAsync();

		/// <summary>
		/// Change point cloud coordinate system to cartesian coordinate.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ SetCartesianCoordinateAsync();

		/// <summary>
		/// Change point cloud coordinate system to spherical coordinate.
		/// </summary>
		/// <returns></returns>
		Task<uint8_t>^ SetSphericalCoordinateAsync();
		
		/// <summary>
		/// Set UTC formate synchronization time.
		/// </summary>
		/// <param name="datetime"></param>
		/// <returns></returns>
		Task<uint8_t>^ LidarSetUtcSyncTimeAsync(DateTime datetime);


		// /// <summary>
		// /// Process Receive data 
		// /// ref @OnReceive[mode]
		// /// </summary>
		// /// <param name="livoxEthPacketData"></param>
		// /// <param name="dataCount"></param>
		// /// <returns></returns>
		// Object^ ProcessReceiveData(IntPtr livoxEthPacketData, uint32_t dataCount); 
		 

#pragma region Events Handlers
		  
		delegate void OnStateUpdateDelegate(LivoxDevice^ device, Def::WrLvDeviceEvent eventType);
		event OnStateUpdateDelegate^ OnStateUpdate
		{
			void add(OnStateUpdateDelegate^ eventHandler)
			{
				onStateUpdateAsync += eventHandler;
			}

			void remove(OnStateUpdateDelegate^ eventHandler)
			{
				onStateUpdateAsync -= eventHandler;
			}
		}

		delegate void OnDeviceInformationDelegate(LivoxDevice^ device, livox_status status, DeviceInformationResponse* ack);
		event OnDeviceInformationDelegate^ OnDeviceInformation
		{
			void add(OnDeviceInformationDelegate^ eventHandler)
			{
				onDeviceInformationAsync += eventHandler;
			}

			void remove(OnDeviceInformationDelegate^ eventHandler)
			{
				onDeviceInformationAsync -= eventHandler;
			}
		}

		delegate void OnErrorMessageDelegate(LivoxDevice^ device, livox_status status, uint32_t errorMessage);
		event OnErrorMessageDelegate^ OnErrorMessage
		{
			void add(OnErrorMessageDelegate^ eventHandler)
			{
				onErrorMessageAsync += eventHandler;
			}

			void remove(OnErrorMessageDelegate^ eventHandler)
			{
				onErrorMessageAsync -= eventHandler;
			}
		}
		
		delegate void OnReceivePointsDelegate(LivoxDevice^ device, IntPtr livoxEthPacketData, uint32_t dataCount);
		event OnReceivePointsDelegate^ OnReceivePoints;

		delegate void OnReceiveRawPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxRawPoint>^ points);
		event OnReceiveRawPointDelegate^ OnReceiveRawPoint;

		delegate void OnReceiveSpherPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxSpherPoint>^ points);
		event OnReceiveSpherPointDelegate^  OnReceiveSpherPoint;
		
		delegate void OnReceiveExtendRawPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxExtendRawPoint>^ points);
		event OnReceiveExtendRawPointDelegate^  OnReceiveExtendRawPoint;
		
		delegate void OnReceiveExtendSpherPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxExtendSpherPoint>^ points);
		event OnReceiveExtendSpherPointDelegate^  OnReceiveExtendSpherPoint;
		 
		delegate void OnReceiveDualExtendRawPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxDualExtendRawPoint>^ points);
		event OnReceiveDualExtendRawPointDelegate^  OnReceiveDualExtendRawPoint;
		
		delegate void OnReceiveDualExtendSpherPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxDualExtendSpherPoint>^ points);
		event OnReceiveDualExtendSpherPointDelegate^  OnReceiveDualExtendSpherPoint;
		
		delegate void OnReceiveImuPointDelegate(LivoxDevice^ device, DateTime arrivedTime, array<Def::LvLivoxImuPoint>^ points);
		event OnReceiveImuPointDelegate^  OnReceiveImuPoint; 
		 
		
		 
		private: 
			OnStateUpdateDelegate^ onStateUpdateAsync;
			OnDeviceInformationDelegate^ onDeviceInformationAsync;
			OnErrorMessageDelegate^ onErrorMessageAsync;

#pragma endregion

	}; 
}