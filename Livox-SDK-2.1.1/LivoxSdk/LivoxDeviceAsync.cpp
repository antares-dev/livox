#include "pch.h"

#include "LivoxSdk.h"
#include "LivoxDevice.h"
#include "Common.h"
#include <stdio.h>


namespace LivoxSdk {

	using namespace LivoxSdk::Def;
	using namespace System;
	using namespace System::Threading;
	using namespace System::Threading::Tasks;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Collections::Concurrent;


	 

	void LidarStartSamplingCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			ld->DoLidarStartSampling(status, response);

			if (status == kStatusSuccess)
			{
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response); 
			}
			else
			{
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
			}
		}
	}

	Task<uint8_t>^ LivoxDevice::LidarStartSamplingAsync()
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (LidarStartSampling(handle, LidarStartSamplingCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}
	 

	void LidarStopSamplingCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			if (status == kStatusSuccess)
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response);
			else
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
		}
	}

	Task<uint8_t>^ LivoxDevice::LidarStopSamplingAsync()
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (LidarStopSampling(handle, LidarStopSamplingCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}


	void SetStaticDynamicIpCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			if (status == kStatusSuccess)
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response);
			else
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
		}
	}
	 
	Task<uint8_t>^ LivoxDevice::SetStaticIpAsync(IPAddress^ ipAddr, IPAddress^ netMask, IPAddress^ gwAddr)
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		SetStaticDeviceIpModeRequest req;
		req.ip_addr = Common::AddreesToInt(ipAddr);
		req.net_mask = Common::AddreesToInt(netMask);
		req.gw_addr = Common::AddreesToInt(gwAddr);

		if (SetStaticIp(handle, &req, SetStaticDynamicIpCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}

	Task<uint8_t>^ LivoxDevice::SetDynamicIpAsync()
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		SetStaticDeviceIpModeRequest req; 

		if (SetDynamicIp(handle, SetStaticDynamicIpCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}
	 
	void DisconnectDeviceCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			if (status == kStatusSuccess)
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response);
			else
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
		}
	}

	Task<uint8_t>^ LivoxDevice::DisconnectDeviceAsync()
	{ 
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (DisconnectDevice(handle, DisconnectDeviceCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}


	void SetCoordinateCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			if (status == kStatusSuccess)
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response);
			else
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
		}
	}

	Task<uint8_t>^ LivoxDevice::SetCartesianCoordinateAsync()
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (SetCartesianCoordinate(handle, SetCoordinateCb, (void *)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}


	Task<uint8_t>^ LivoxDevice::SetSphericalCoordinateAsync()
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		if (SetSphericalCoordinate(handle, SetCoordinateCb, (void *)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}


	void LidarSetUtcSyncTimeCb(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		int clientId = (int)client_data;
		Livox^ l = Livox::GetInstance();
		LivoxDevice^ ld = l->GetDeviceByHandle(handle);
		if (ld != nullptr)
		{
			if (status == kStatusSuccess)
				ld->TasksSource->TrySetResult<uint8_t>(clientId, response);
			else
				ld->TasksSource->TrySetException<uint8_t>(clientId, "livox_status: " + status);
		}
	}

	Task<uint8_t>^ LivoxDevice::LidarSetUtcSyncTimeAsync(DateTime datetime)
	{
		int clientId = TasksSource->GenNewId();
		Task<uint8_t>^ task = TasksSource->AddTask<uint8_t>(clientId);

		datetime = datetime.ToUniversalTime();

		LidarSetUtcSyncTimeRequest req;
		req.year = (datetime.Year % 100);
		req.month = datetime.Month;
		req.day = datetime.Day;
		req.hour = datetime.Hour;
		req.mircrosecond = (datetime.Minute * 60e6 + datetime.Second * 1e6 + datetime.Millisecond * 1e3);

		if (LidarSetUtcSyncTime(handle, &req, LidarSetUtcSyncTimeCb, (void*)clientId) != kStatusSuccess)
		{
			TasksSource->TrySetCanceled<uint8_t>(clientId);
		}
		return task;
	}
}