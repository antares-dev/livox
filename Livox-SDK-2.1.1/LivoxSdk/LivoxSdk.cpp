#include "pch.h"

#include "LivoxSdk.h"
#include "LivoxSdkDef.h" 
#include "Common.h" 
#include <stdio.h>


namespace LivoxSdk {

	Livox::Livox()
	{
		// Constructor
		devicesIDFilter = gcnew array <String^>(0);
	}

	Livox::~Livox()
	{
		// Delete
	}

	Livox^ Livox::GetInstance()
	{
		if (livoxInstance == nullptr)
		{
			livoxInstance = gcnew Livox();
		}
		return livoxInstance;
	}
	  

	LivoxDevice^ Livox::AddDevice(String^ deviceId)
	{
		auto device = GetDevice(deviceId);

		if (device == nullptr && devices->Count < 100)
		{
			device = gcnew LivoxDevice(255, deviceId);
			devices->Add(device);
		}

		return device;
	};


	LivoxDevice^ Livox::GetOrAddDevice(uint8_t handle, String^ deviceId)
	{
		auto device = GetDeviceByHandle(handle);

		if (device == nullptr && handle != 255)
		{
			// Update handle if need
			device = GetDevice(deviceId);
			if (device != nullptr && !device->ContainHandle())
			{
				device->SetHandle(handle);
			}
			else
			{
				device = nullptr;
			}
		}

		if (device == nullptr && handle != 255 && devices->Count < 100)
		{
			device = gcnew LivoxDevice(handle, deviceId);
			devices->Add(device);
		}

		return device;
	}


	LivoxDevice^ Livox::GetDeviceByHandle(uint8_t handle)
	{
		for each (auto device in devices)
		{
			if (device->GetHandle() == handle)
				return device;
		}

		return nullptr;
	}

	LivoxDevice^ Livox::GetDevice(String^ deviceId)
	{
		for each (auto device in devices)
		{
			if (device->GetId() == deviceId)
				return device;
		}

		return nullptr;
	}


	Def::WrLvSdkVersion^ Livox::LvGetLivoxSdkVersion()
	{
		LivoxSdkVersion version;
		GetLivoxSdkVersion(&version);

		Def::WrLvSdkVersion^ n = gcnew Def::WrLvSdkVersion(&version);
		return n;
	}


	void Livox::LvDisableConsoleLogger()
	{
		DisableConsoleLogger();
	}


	bool Livox::LvInit()
	{
		if (isInitialized)
			return true;

		bool result = Init();

		if (result)
		{
			SetBroadcastCallback(OnInstanceDeviceBroadcastCB);
			SetDeviceStateUpdateCallback(OnInstanceDeviceStateUpdateCB);
		}

		isInitialized = result;
		return result;
	}

	bool Livox::LvStart()
	{
		return Start();
	}

	

	array<Def::WrLvDeviceInfo^>^ Livox::LvGetConnectedDevices()
	{
		uint8_t  size = DevicesCount();
		DeviceInfo* devices = new DeviceInfo[size];

		GetConnectedDevices(devices, &size);

		auto devs = Common::ParseData<Def::WrLvDeviceInfo, DeviceInfo*>(devices, size);
		delete[] devices;

		return devs;
	}

	array<LivoxDevice^>^ Livox::GetLivoxDevices()
	{
		return devices->ToArray();
	}
	  
	livox_status Livox::LvAddLidarToConnect(String^ broadcastCode)
	{
		auto device = AddDevice(broadcastCode);
		if (device != nullptr && !device->IsConnected())
		{
			return	device->LvAddLidarToConnect();
		}
		return kStatusFailure;
	}
	
	livox_status Livox::LvAddHubToConnect(String^ broadcastCode)
	{
		auto device = AddDevice(broadcastCode);
		if (device != nullptr && !device->IsConnected())
		{
			return	device->LvAddHubToConnect();
		}
		return kStatusFailure;
	}

	void Livox::LvUninit()
	{
		Uninit();
	}

	void Livox::LvSaveLoggerFile()
	{
		SaveLoggerFile();
	}


	bool Livox::LvLidarStartAutoConnect(array<String^>^ devicesID)
	{  
		// Registrar DeviceID
		this->devicesIDFilter = devicesID;

		for each (String^ devId in devicesID)
		{
			AddDevice(devId);
		}

		// Initialize SDK
		bool result = isInitialized; 
		if (!result)
		{
			result = LvInit();
		}

		if (result)
		{
			result = LvStart();
		}  

		isLidarAutoConnect = true;
		return result;
	}
	 
	bool Livox::IsAutoConnectMode()
	{
		return devicesIDFilter->Length == 0 && (isLidarAutoConnect || isHubAutoConnect);
	}

	bool Livox::ExistInWhitelist(String^ deviceId)
	{
		int idx = Array::IndexOf(devicesIDFilter, deviceId);
		return idx >= 0;
	} 


	void Livox::DoDeviceBroadcast(const BroadcastDeviceInfo* info)
	{
		Def::WrLvBroadcastDeviceInfo^ infoNew = gcnew Def::WrLvBroadcastDeviceInfo((BroadcastDeviceInfo*)info);

		if (onDeviceBroadcastAsync != nullptr)
			onDeviceBroadcastAsync->BeginInvoke(infoNew, nullptr, nullptr);

		if (info == NULL)
		{
			return;
		}

		if (info->dev_type == kDeviceTypeHub && isLidarAutoConnect)
		{
			printf("In lidar mode, couldn't connect a hub : %s\n", info->broadcast_code);
			return;
		}
		else if (info->dev_type != kDeviceTypeHub && isHubAutoConnect)
		{
			printf("In hub mode, couldn't connect a lidar : %s\n", info->broadcast_code);
			return;
		}

		auto device = GetDevice(infoNew->broadcast_code);
		if (device != nullptr && device->IsConnected())
		{
			return;
		}

		if (IsAutoConnectMode())
		{
			printf("In automatic connection mode, will connect %s\n", info->broadcast_code);
		}
		else if (ExistInWhitelist(infoNew->broadcast_code))
		{
			printf("In automatic connection mode from whitelist %s\n", info->broadcast_code);
		}
		else
		{
			printf("Not in the whitelist, please add %s to if want to connect!\n", info->broadcast_code);
			return;
		}

		if (isLidarAutoConnect)
		{
			LvAddLidarToConnect(infoNew->broadcast_code);
		}

		if (isHubAutoConnect)
		{
			LvAddHubToConnect(infoNew->broadcast_code);
		}
	};

	void Livox::DoDeviceStateUpdate(const DeviceInfo* deviceInfo, DeviceEvent eventType)
	{
		auto deviceNew = gcnew Def::WrLvDeviceInfo(deviceInfo);
		auto eventTypeNew = (Def::WrLvDeviceEvent)eventType;
		 
		if (IsAutoConnectMode() || ExistInWhitelist(deviceNew->broadcast_code))
		{
			// Add / Update Device to stack
			LivoxDevice^ device = GetOrAddDevice(deviceNew->handle, deviceNew->broadcast_code);
			if (device != nullptr)
			{
				if (eventTypeNew == Def::WrLvDeviceEvent::kEventConnect)
				{
					if (AutoConnectCartesianCoordinate)
					{
						device->SetCartesianCoordinateAsync();
					}
					else
					{
						device->SetSphericalCoordinateAsync();
					}

					device->LidarStartSamplingAsync();
				}
			}
		}

		if (onDeviceStateUpdateAsync != nullptr)
			onDeviceStateUpdateAsync->BeginInvoke(deviceNew, eventTypeNew, nullptr, nullptr);

		auto device = GetDeviceByHandle(deviceNew->handle);
		if (device != nullptr)
		{
			device->DoStateUpdate(deviceNew, eventTypeNew);
		}
	}


	void Livox::DoCommonCommand(livox_status status, uint8_t handle, uint8_t response, void* client_data)
	{
		if (onCommonCommandAsync != nullptr)
			onCommonCommandAsync->BeginInvoke(status, handle, response, nullptr, nullptr);
	}
}
