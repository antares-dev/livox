#pragma once

#include "livox_sdk.h"
#include "LivoxSdkDef.h"
#include "StaticCollbacks.h"
#include "LivoxDevice.h" 
#include "TaskSourceHelper.h" 

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Threading;

namespace LivoxSdk {

	public ref class Livox
	{
	private:
		static Livox^ livoxInstance;
		List<LivoxDevice^>^ devices = gcnew List<LivoxDevice^>();
		array<String^>^ devicesIDFilter;
		bool isInitialized = false;
		bool isLidarAutoConnect = false;
		bool isHubAutoConnect = false;

		Livox();
		~Livox();

	internal:
		TaskSourceHelper^ TasksSource = gcnew TaskSourceHelper();

		void DoDeviceBroadcast(const BroadcastDeviceInfo* info);
		void DoDeviceStateUpdate(const DeviceInfo* device, DeviceEvent type);
		void DoCommonCommand(livox_status status, uint8_t handle, uint8_t response, void* client_data);
		 
		LivoxDevice^ AddDevice(String^ deviceId);
		LivoxDevice^ GetOrAddDevice(uint8_t handle, String^ deviceId);


	public:

		/// <summary>
		/// Single Instance of Livox Wrapper
		/// </summary>
		/// <returns></returns>
		static Livox^ GetInstance();


		/// <summary>
		/// Get configurated device by handle
		/// </summary>
		/// <param name="handle"></param>
		/// <returns></returns>
		LivoxDevice^ GetDeviceByHandle(uint8_t handle);

		/// <summary>
		/// Get configurated device by device name id
		/// </summary>
		/// <param name="deviceId"></param>
		/// <returns></returns>
		LivoxDevice^ GetDevice(String^ deviceId);

		/// <summary>
		/// Configurated devices count
		/// </summary>
		/// <returns></returns>
		int DevicesCount() { return devices->Count; }

		/// <summary>
		/// Return SDK's version information in a numeric form.
		/// @param version Pointer to a version structure for returning the version information.
		/// </summary>
		/// <returns></returns>
		Def::WrLvSdkVersion^ LvGetLivoxSdkVersion();

		/// <summary>
		/// Disable console log output.
		/// </summary>
		void LvDisableConsoleLogger();
		 

		/// <summary>
		/// Initialize the SDK.
		/// </summary>
		/// <returns>true if successfully initialized, otherwise false.</returns>
		bool LvInit();


		/// <summary>
		/// Start the device scanning routine which runs on a separate thread.
		/// </summary>
		/// <returns>true if successfully started, otherwise false.</returns>
		bool LvStart();


		/// <summary>
		/// Uninitialize the SDK
		/// </summary>
		void LvUninit();


		/// <summary>
		///  Save the log file.
		/// </summary>
		void LvSaveLoggerFile();


		/// <summary>
		/// @c SetBroadcastCallback response callback function.
		/// @param info information of the broadcast device, becomes invalid after the function returns.
		/// </summary>
		delegate void OnDeviceBroadcastDelegate(Def::WrLvBroadcastDeviceInfo^ info);


		/// <summary>
		/// Set the callback of listening device broadcast message. 
		/// When broadcast message is received from Livox Hub/LiDAR, cb is called. 
		/// see @OnDeviceBroadcastDelegate
		/// </summary> 
		event OnDeviceBroadcastDelegate^ OnDeviceBroadcast
		{
			void add(OnDeviceBroadcastDelegate^ eventHandler)
			{
				onDeviceBroadcastAsync += eventHandler;
			}

			void remove(OnDeviceBroadcastDelegate^ eventHandler)
			{
				onDeviceBroadcastAsync -= eventHandler;
			}
		}

		/// <summary> 
		/// @c SetDeviceStateUpdateCallback response callback function.
		/// @param device  information of the connected device.
		/// @param type    the update type that indicates connection/disconnection of the device or change of working state.
		/// </summary>
		delegate void OnDeviceStateUpdateDelegate(Def::WrLvDeviceInfo^ deviceInfo, Def::WrLvDeviceEvent eventType);

		/// <summary> 
		/// @brief Add a callback for device connection or working state changing event.
		/// 	@note Livox SDK supports two hardware connection modes. 1: Directly connecting to the LiDAR device; 2. Connecting to
		/// 	the LiDAR device(s) via the Livox Hub.In the first mode, connection / disconnection of every LiDAR unit is reported by
		/// 	this callback.In the second mode, only connection / disconnection of the Livox Hub is reported by this callback.If
		/// 	you want to get information of the LiDAR unit(s) connected to hub, see \ref HubQueryLidarInformation.
		/// 	@note 3 conditions can trigger this callback:
		/// 1. Connection and disconnection of device.
		/// 	2. A change of device working state.
		/// 	3. An error occurs.
		/// 	@param cb callback for device connection / disconnection.
		/// </summary>  
		event OnDeviceStateUpdateDelegate^ OnDeviceStateUpdate
		{
			void add(OnDeviceStateUpdateDelegate^ eventHandler)
			{
				onDeviceStateUpdateAsync += eventHandler;
			}

			void remove(OnDeviceStateUpdateDelegate^ eventHandler)
			{
				onDeviceStateUpdateAsync -= eventHandler;
			}
		}

		delegate void OnCommonCommandDelegate(uint32_t status, uint8_t handle, uint8_t response);
		 
		event OnCommonCommandDelegate^ OnCommonCommand
		{
			void add(OnCommonCommandDelegate^ eventHandler)
			{
				onCommonCommandAsync += eventHandler;
			}

			void remove(OnCommonCommandDelegate^ eventHandler)
			{
				onCommonCommandAsync -= eventHandler;
			}
		}

		/// <summary>
		/// Get all connected devices' information.
		/// </summary>
		/// <returns></returns>
		array< Def::WrLvDeviceInfo^>^ LvGetConnectedDevices();

		/// <summary>
		/// Get Livox devices registered in auto mode
		/// or added with: LvAddLidarToConnect | LvAddHubToConnect
		/// </summary>
		/// <returns></returns>
		array<LivoxDevice^>^ GetLivoxDevices();


		/// <summary>
		/// Add a broadcast code to the connecting list and only devices with broadcast code in this list will be connected
		/// Is possible obtain device with:
		///		device = @GetDevice(<broadcastCode>)
		/// </summary>
		/// <returns></returns>
		livox_status LvAddLidarToConnect(String^ broadcastCode);


		/// <summary>
		/// Add a broadcast code to the connecting list and only devices with broadcast code in this list will be connected. 
		/// The broadcast code is unique for every device.
		/// Is possible obtain device with:
		///		device = @GetDevice(<broadcastCode>)
		/// </summary>
		/// <param name="broadcastCode"></param>
		/// <returns></returns>
		livox_status LvAddHubToConnect(String^ broadcastCode);

		/// <summary>
		/// Auto connect mode ref @LvLidarStartAutoConnect
		/// </summary>
		/// <returns></returns>
		bool IsAutoConnectMode();

		/// <summary>
		/// Exist in whitelist ref @LvLidarStartAutoConnect
		/// </summary>
		/// <param name="deviceId"></param>
		/// <returns></returns>
		bool ExistInWhitelist(String^ deviceId);

		/// <summary>
		/// On Auto Connect mode use cartesian coordinate if set, otherwise spherical
		/// </summary>
		bool AutoConnectCartesianCoordinate = true;

		/// <summary>
		/// Init and Start LiDAR sampling (Auto Connect) and Sampling based on devicesID filter
		/// </summary>
		/// <param name="devicesID">filter Whitelist</param>
		/// <returns></returns>
		bool LvLidarStartAutoConnect(array<String^>^ devicesID);

		/// <summary>
		/// Start hub sampling.
		/// </summary>
		/// <returns>Task with result from callback response</returns>
		Task<uint8_t>^ HubStartSamplingAsync();

		/// <summary>
		/// Stop the Livox Hub's sampling.
		/// </summary>
		/// <returns>Task with result from callback response</returns>
		Task<uint8_t>^ HubStopSamplingAsync();
		 

		array<Def::LvLivoxRawPoint>^ GetLivoxRawPointDemoNative(int len);

		array<Def::LvLivoxRawPoint>^ GetLivoxRawPointDemoNative2(int len);
		  
		array<Def::LvLivoxRawPoint>^ GetLivoxRawPointDemo();


	protected:
		OnDeviceStateUpdateDelegate^ onDeviceStateUpdateAsync;
		OnDeviceBroadcastDelegate^ onDeviceBroadcastAsync; 
		OnCommonCommandDelegate^ onCommonCommandAsync; 
	};
}
