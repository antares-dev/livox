#pragma once

#include "livox_def.h"
 

namespace LivoxSdk {
	namespace Def {

		using namespace System;
		using namespace System::Runtime::InteropServices;

#pragma region livox_def enums 

		/** Device update type, indicating the change of device connection or working state. */
		public enum class WrLvDeviceEvent
		{
			kEventConnect = 0,              /**< Device is connected. */
			kEventDisconnect = 1,           /**< Device is removed. */
			kEventStateChange = 2,          /**< Device working state changes or an error occurs. */
			kEventHubConnectionChange = 3   /**< Hub is connected or LiDAR unit(s) is/are removed. */
		};

		public enum class WrLvDeviceState {
			kDeviceStateDisconnect = 0,
			kDeviceStateConnect = 1,
			kDeviceStateSampling = 2,
		};

		/** Device type. */
		public enum class WrLvDeviceType
		{
			kDeviceTypeHub = 0,          /**< Livox Hub. */
			kDeviceTypeLidarMid40 = 1,   /**< Mid-40. */
			kDeviceTypeLidarTele = 2,    /**< Tele. */
			kDeviceTypeLidarHorizon = 3  /**< Horizon. */
		};

		/** Lidar state. */
		public enum class WrLvLidarState
		{
			kLidarStateInit = 0,        /**< Initialization state. */
			kLidarStateNormal = 1,      /**< Normal work state. */
			kLidarStatePowerSaving = 2, /**< Power-saving state. */
			kLidarStateStandBy = 3,     /**< Standby state. */
			kLidarStateError = 4,       /**< Error state. */
			kLidarStateUnknown = 5      /**< Unknown state. */
		};
		 


#pragma endregion


#pragma region Union

		/** Information of LiDAR work state. */
		public ref class WrLvStatusUnion
		{
		public:
			uint32_t progress;    /**< LiDAR work state switching progress. */
			//ErrorMessage status_code; /**< LiDAR work state status code. */
			uint32_t error_code;                /**< Error code. */

			WrLvStatusUnion(StatusUnion* version)
			{
				this->progress = version->progress;
				this->error_code = version->status_code.error_code;
			}
		};

#pragma endregion


#pragma region livox_def structs 
		/** Cartesian coordinate format. */
		 

		/** Cartesian coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxRawPoint {
			int32_t x;            /**< X axis, Unit:mm */
			int32_t y;            /**< Y axis, Unit:mm */
			int32_t z;            /**< Z axis, Unit:mm */
			uint8_t reflectivity; /**< Reflectivity */
		} ;


		/** Spherical coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxSpherPoint {
			uint32_t depth;       /**< Depth, Unit: mm */
			uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
			uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
			uint8_t reflectivity; /**< Reflectivity */
		} ;

		/** Extend cartesian coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxExtendRawPoint{
			int32_t x;            /**< X axis, Unit:mm */
			int32_t y;            /**< Y axis, Unit:mm */
			int32_t z;            /**< Z axis, Unit:mm */
			uint8_t reflectivity; /**< Reflectivity */
			uint8_t tag;          /**< Tag */
		} ;

		/** Extend spherical coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxExtendSpherPoint {
			uint32_t depth;       /**< Depth, Unit: mm */
			uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
			uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
			uint8_t reflectivity; /**< Reflectivity */
			uint8_t tag;          /**< Tag */
		} ;

		/** Dual extend cartesian coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxDualExtendRawPoint {
			int32_t x1;            /**< X axis, Unit:mm */
			int32_t y1;            /**< Y axis, Unit:mm */
			int32_t z1;            /**< Z axis, Unit:mm */
			uint8_t reflectivity1; /**< Reflectivity */
			uint8_t tag1;          /**< Tag */
			int32_t x2;            /**< X axis, Unit:mm */
			int32_t y2;            /**< Y axis, Unit:mm */
			int32_t z2;            /**< Z axis, Unit:mm */
			uint8_t reflectivity2; /**< Reflectivity */
			uint8_t tag2;          /**< Tag */
		} ;


		/** Dual extend spherical coordinate format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxDualExtendSpherPoint {
			uint16_t theta;        /**< Zenith angle[0, 18000], Unit: 0.01 degree */
			uint16_t phi;          /**< Azimuth[0, 36000], Unit: 0.01 degree */
			uint32_t depth1;       /**< Depth, Unit: mm */
			uint8_t reflectivity1; /**< Reflectivity */
			uint8_t tag1;          /**< Tag */
			uint32_t depth2;       /**< Depth, Unit: mm */
			uint8_t reflectivity2; /**< Reflectivity */
			uint8_t tag2;          /**< Tag */
		} ;

		/** IMU data format. */
		[StructLayout(LayoutKind::Sequential, Pack = 1)]
		public value struct LvLivoxImuPoint {
			float gyro_x;        /**< Gyroscope X axis, Unit:rad/s */
			float gyro_y;        /**< Gyroscope Y axis, Unit:rad/s */
			float gyro_z;        /**< Gyroscope Z axis, Unit:rad/s */
			float acc_x;         /**< Accelerometer X axis, Unit:g */
			float acc_y;         /**< Accelerometer Y axis, Unit:g */
			float acc_z;         /**< Accelerometer Z axis, Unit:g */
		} ; 

		public value struct ManagedEmp {
			System::String^ name;
			System::String^ address;
			int zipCode;
		}; 


		//// /** Cartesian coordinate format. */
		//// public ref class WrLvLivoxRawPoint
		//// {
		//// public:
		//// 	int32_t x;            /**< X axis, Unit:mm */
		//// 	int32_t y;            /**< Y axis, Unit:mm */
		//// 	int32_t z;            /**< Z axis, Unit:mm */
		//// 	uint8_t reflectivity; /**< Reflectivity */
		//// 
		//// 	WrLvLivoxRawPoint(LivoxRawPoint* point)
		//// 	{
		//// 		this->x = point->x;
		//// 		this->y = point->y;
		//// 		this->z = point->z;
		//// 		this->reflectivity = point->reflectivity;
		//// 	}
		//// };
		//// 
		//// /** Extend cartesian coordinate format. */
		//// public ref class WrLvLivoxExtendRawPoint
		//// {
		//// public:
		//// 	int32_t x;            /**< X axis, Unit:mm */
		//// 	int32_t y;            /**< Y axis, Unit:mm */
		//// 	int32_t z;            /**< Z axis, Unit:mm */
		//// 	uint8_t reflectivity; /**< Reflectivity */
		//// 	uint8_t tag;          /**< Tag */
		//// 
		//// 	WrLvLivoxExtendRawPoint(LivoxExtendRawPoint* point)
		//// 	{
		//// 		this->x = point->x;
		//// 		this->y = point->y;
		//// 		this->z = point->z;
		//// 		this->reflectivity = point->reflectivity;
		//// 		this->tag = point->tag;
		//// 	}
		//// };
		//// 
		//// /** Dual extend cartesian coordinate format. */
		//// public ref class WrLvLivoxDualExtendRawPoint
		//// {
		//// public:
		//// 	int32_t x1;            /**< X axis, Unit:mm */
		//// 	int32_t y1;            /**< Y axis, Unit:mm */
		//// 	int32_t z1;            /**< Z axis, Unit:mm */
		//// 	uint8_t reflectivity1; /**< Reflectivity */
		//// 	uint8_t tag1;          /**< Tag */
		//// 	int32_t x2;            /**< X axis, Unit:mm */
		//// 	int32_t y2;            /**< Y axis, Unit:mm */
		//// 	int32_t z2;            /**< Z axis, Unit:mm */
		//// 	uint8_t reflectivity2; /**< Reflectivity */
		//// 	uint8_t tag2;          /**< Tag */
		//// 
		//// 	WrLvLivoxDualExtendRawPoint(LivoxDualExtendRawPoint* point)
		//// 	{
		//// 		this->x1 = point->x1; 
		//// 		this->y1 = point->y1; 
		//// 		this->z1 = point->z1; 
		//// 		this->reflectivity1 = point->reflectivity1; 
		//// 		this->tag1 = point->tag1; 
		//// 		this->x2 = point->x2;
		//// 		this->y2 = point->y2;
		//// 		this->z2 = point->z2;
		//// 		this->reflectivity2 = point->reflectivity2;
		//// 		this->tag2 = point->tag2;
		//// 	}
		//// };
		//// 
		//// /** Spherical coordinate format. */
		//// public ref class WrLvLivoxSpherPoint
		//// {
		//// public:
		//// 
		//// 	uint32_t depth;       /**< Depth, Unit: mm */
		//// 	uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
		//// 	uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
		//// 	uint8_t reflectivity; /**< Reflectivity */
		//// 
		//// 	WrLvLivoxSpherPoint(LivoxSpherPoint* point)
		//// 	{
		//// 		this->depth = point->depth;
		//// 		this->theta = point->theta;
		//// 		this->phi = point->phi;
		//// 		this->reflectivity = point->reflectivity;
		//// 	}
		//// };
		//// 
		//// 
		//// /** Extend spherical coordinate format. */
		//// public ref class WrLvLivoxExtendSpherPoint
		//// {
		//// public:
		//// 
		//// 	uint32_t depth;       /**< Depth, Unit: mm */
		//// 	uint16_t theta;       /**< Zenith angle[0, 18000], Unit: 0.01 degree */
		//// 	uint16_t phi;         /**< Azimuth[0, 36000], Unit: 0.01 degree */
		//// 	uint8_t reflectivity; /**< Reflectivity */
		//// 	uint8_t tag;          /**< Tag */
		//// 
		//// 	WrLvLivoxExtendSpherPoint(LivoxExtendSpherPoint* point)
		//// 	{
		//// 		this->depth = point->depth;
		//// 		this->theta = point->theta;
		//// 		this->phi = point->phi;
		//// 		this->reflectivity = point->reflectivity;
		//// 		this->tag = point->tag;
		//// 	}
		//// };
		//// 
		//// /** Dual extend spherical coordinate format. */
		//// public ref class WrLvLivoxDualExtendSpherPoint
		//// {
		//// public:
		//// 	uint16_t theta;        /**< Zenith angle[0, 18000], Unit: 0.01 degree */
		//// 	uint16_t phi;          /**< Azimuth[0, 36000], Unit: 0.01 degree */
		//// 	uint32_t depth1;       /**< Depth, Unit: mm */
		//// 	uint8_t reflectivity1; /**< Reflectivity */
		//// 	uint8_t tag1;          /**< Tag */
		//// 	uint32_t depth2;       /**< Depth, Unit: mm */
		//// 	uint8_t reflectivity2; /**< Reflectivity */
		//// 	uint8_t tag2;          /**< Tag */
		//// 
		//// 	WrLvLivoxDualExtendSpherPoint(LivoxDualExtendSpherPoint* point)
		//// 	{
		//// 		this->theta = point->theta;
		//// 		this->phi = point->phi;
		//// 		this->depth1 = point->depth1;
		//// 		this->reflectivity1 = point->reflectivity1;
		//// 		this->tag1 = point->tag1;
		//// 		this->depth2 = point->depth2;  
		//// 		this->reflectivity2 = point->reflectivity2;
		//// 		this->tag2 = point->tag2;
		//// 	}
		//// };
		//// 
		//// 
		//// /** IMU data format. */
		//// public ref class WrLvLivoxImuPoint
		//// {
		//// public: 
		//// 		float gyro_x;        /**< Gyroscope X axis, Unit:rad/s */
		//// 		float gyro_y;        /**< Gyroscope Y axis, Unit:rad/s */
		//// 		float gyro_z;        /**< Gyroscope Z axis, Unit:rad/s */
		//// 		float acc_x;         /**< Accelerometer X axis, Unit:g */
		//// 		float acc_y;         /**< Accelerometer Y axis, Unit:g */
		//// 		float acc_z;         /**< Accelerometer Z axis, Unit:g */
		////    
		//// 	WrLvLivoxImuPoint(LivoxImuPoint* point)
		//// 	{
		//// 		this->gyro_x = point->gyro_x; 
		//// 		this->gyro_y = point->gyro_y; 
		//// 		this->gyro_z = point->gyro_z; 
		//// 		this->acc_x = point->acc_x;
		//// 		this->acc_y = point->acc_y;
		//// 		this->acc_z = point->acc_z;
		//// 	}
		//// };

		/// <summary>
		/// The numeric version information struct.
		/// </summary>
		public ref class WrLvSdkVersion
		{
		public:
			int major;      /**< major number */
			int minor;      /**< minor number */
			int patch;      /**< patch number */

			WrLvSdkVersion(LivoxSdkVersion* version)
			{
				this->major = version->major;
				this->minor = version->minor;
				this->patch = version->patch;
			}
		};


		/** The information of broadcast device. */
		public ref class WrLvBroadcastDeviceInfo
		{
		public:
			String^ broadcast_code; /**< Device broadcast code, null-terminated string, 15 characters at most. */
			WrLvDeviceType dev_type;       /**< Device type, refer to \ref DeviceType. */
			uint16_t reserved;      /**< Reserved. */
			String^ ip;            /**< Device ip. */

			WrLvBroadcastDeviceInfo(BroadcastDeviceInfo* devInfo)
			{
				this->broadcast_code = gcnew String(devInfo->broadcast_code);
				this->dev_type = (WrLvDeviceType)devInfo->dev_type;
				this->reserved = devInfo->reserved;
				this->ip = gcnew String(devInfo->ip);
			}
		};


		public ref class WrLvDeviceInfo
		{
		public:

			String^ broadcast_code;       /**< Device broadcast code, null-terminated string, 15 characters at most. */
			uint8_t handle;               /**< Device handle. */
			uint8_t slot;                 /**< Slot number used for connecting LiDAR. */
			uint8_t id;                   /**< LiDAR id. */
			uint8_t type;                 /**< Device type, refer to \ref DeviceType. */
			uint16_t data_port;           /**< Point cloud data UDP port. */
			uint16_t cmd_port;            /**< Control command UDP port. */
			uint16_t sensor_port;         /**< IMU data UDP port. */
			String^ ip;                   /**< IP address. */
			WrLvLidarState state;             /**< LiDAR state. */
			LidarFeature feature;         /**< LiDAR feature. */
			int32_t status;          /**< LiDAR work state status. */
			uint8_t* firmware_version;     /**< Firmware version. */

			WrLvDeviceInfo(const DeviceInfo* devInfo)
			{
				this->broadcast_code = gcnew String(devInfo->broadcast_code);
				this->handle = devInfo->handle;
				this->slot = devInfo->slot;
				this->id = devInfo->id;
				this->type = devInfo->type;
				this->data_port = devInfo->data_port;
				this->cmd_port = devInfo->cmd_port;
				this->sensor_port = devInfo->sensor_port;
				this->ip = gcnew String(devInfo->ip);
				this->state = (WrLvLidarState)(devInfo->state);
				this->feature = devInfo->feature;
				// this->status = *((int32_t *)&(devInfo->status));
				// this->firmware_version = devInfo->firmware_version;
			}
		};

#pragma endregion

	}
}