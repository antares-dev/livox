#include "pch.h"

#include "LivoxSdk.h"
#include "StaticCollbacks.h"
  
using namespace System;

namespace LivoxSdk {

    using namespace LivoxSdk::Def;

    void OnInstanceDeviceBroadcastCB(const BroadcastDeviceInfo* info)
    {
        Livox^ l = Livox::GetInstance();
        l->DoDeviceBroadcast(info);
    }
    
    void OnInstanceDeviceStateUpdateCB(const DeviceInfo* device, DeviceEvent eventType)
    {
        Livox^ l = Livox::GetInstance();
        l->DoDeviceStateUpdate(device, eventType);
    }

    void OnInstanceCommonCommandCB(livox_status status, uint8_t handle, uint8_t response, void* client_data)
    {
        Livox^ l = Livox::GetInstance();
        l->DoCommonCommand(status, handle, response, client_data); 
    }
     
    void OnInstanceReciveDataCB(uint8_t handle, LivoxEthPacket* data, uint32_t data_num, void* client_data)
    {
        Livox^ l = Livox::GetInstance();
        LivoxDevice^ ld = l->GetDeviceByHandle(handle);
        if (ld != nullptr)  ld->DoReceiveData(data, data_num);
    }
     
    void OnInstanceDeviceInformationCB(livox_status status, uint8_t handle, DeviceInformationResponse* response, void* client_data)
    {
        Livox^ l = Livox::GetInstance();
        LivoxDevice^ ld = l->GetDeviceByHandle(handle);
        if (ld != nullptr) ld->DoDeviceInformation(status, response, client_data);
    }

    void OnInstanceErrorMessageCB(livox_status status, uint8_t handle, ErrorMessage* message)
    {
        Livox^ l = Livox::GetInstance();
        LivoxDevice^ ld = l->GetDeviceByHandle(handle);
        if (ld != nullptr) ld->DoErrorMessage(status, message);
    } 
}
