#pragma once
 
#include "livox_sdk.h"
#include "LivoxSdkDef.h"

using namespace System; 

namespace LivoxSdk {

	void OnInstanceDeviceBroadcastCB(const BroadcastDeviceInfo* info);

	void OnInstanceReciveDataCB(uint8_t handle, LivoxEthPacket* data, uint32_t data_num, void* client_data);

	void OnInstanceDeviceStateUpdateCB(const DeviceInfo* device, DeviceEvent eventType);

	void OnInstanceCommonCommandCB(livox_status status, uint8_t handle, uint8_t response, void* client_data);
	
	void OnInstanceDeviceInformationCB(livox_status status, uint8_t handle, DeviceInformationResponse* response, void* client_data);
	  
	void OnInstanceErrorMessageCB(livox_status status, uint8_t handle, ErrorMessage* message);
}