#pragma once
#include "pch.h"    

using namespace System;
using namespace System::Threading::Tasks;
using namespace System::Collections::Concurrent;
using namespace System::Threading;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Collections::Concurrent;


namespace LivoxSdk {

	ref struct TaskSourceDate
	{
	public:
		TaskSourceDate(Object^ taskSource)
		{
			this->taskSourceObject = taskSource;
			this->generated = DateTime::Now;
		};

		Object^ taskSourceObject;
		DateTime generated;
	};
	 
	  
	public ref class TaskSourceHelper
	{ 
	public:
		ConcurrentDictionary<int, TaskSourceDate^>^ tasks;

		TaskSourceHelper() 
		{
			tasks = gcnew ConcurrentDictionary<int, TaskSourceDate^>();
		}

		int GenNewId() { static int _id = 1000; return _id++; }

		template <typename TResult>
		TaskCompletionSource<TResult>^ GetTaskSource(int clientDataTask)
		{
			TaskSourceDate^ taskSrcObj;

			if (tasks->TryRemove(clientDataTask, taskSrcObj))
			{
				if (taskSrcObj->taskSourceObject->GetType() == TaskCompletionSource<TResult>::typeid)
				{
					TaskCompletionSource<TResult>^ taskSrc = ((TaskCompletionSource<TResult>^)(taskSrcObj->taskSourceObject));
					return taskSrc;
				}
			}
			return nullptr;
		};
		   

		void CleanTaskCompleted()
		{ 
			DateTime now = DateTime::Now;
			TaskSourceDate^ taskSrcObj;
			for each (auto t in tasks->ToArray())
			{
				if ((now - t.Value->generated).TotalSeconds > 5)
				{
					tasks->TryRemove(t.Key, taskSrcObj);

					try {
						Type^ typ = taskSrcObj->taskSourceObject->GetType(); 
						auto method = typ->GetMethod("SetCanceled");
						method->Invoke(taskSrcObj->taskSourceObject, nullptr);
					}
					catch (int e) {} 
				}
			}
		}


		template <typename TResult1>
		Task<TResult1>^ AddTask(int clientId)
		{
			TaskCompletionSource<TResult1>^ ts = gcnew TaskCompletionSource<TResult1>();
			 
			CleanTaskCompleted();
			 
			tasks->TryAdd(clientId, gcnew TaskSourceDate(ts));

			return  ts->Task;
		}

		template <typename TResult2>
		Task<TResult2>^ GetTask(int clientId)
		{
			TaskCompletionSource<TResult2>^ ts = GetTaskSource<TResult2>(clientId); 
			return ts->Task;
		};

		template <typename TResult3>
		bool TrySetResult(int clientId, TResult3 result)
		{
			TaskCompletionSource<TResult3>^ ts = GetTaskSource<TResult3>(clientId);
			return  ts == nullptr ? false : ts->TrySetResult(result);
		}

		template <typename TResult4>
		bool TrySetException(int clientId, String^ exception)
		{
			TaskCompletionSource<TResult4>^ ts = GetTaskSource<TResult4>(clientId);
			return  ts == nullptr ? false : ts->TrySetException(gcnew Exception(exception));
		}

		template<typename TResult5>
		bool TrySetCanceled(int clientId)
		{
			TaskCompletionSource<TResult5>^ ts = GetTaskSource<TResult5>(clientId);
			return ts == nullptr ? false : ts->TrySetCanceled();
		}

	}; 

}